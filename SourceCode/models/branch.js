const mongoose = require('mongoose');
const Location = require('./location');

//branch schema
const branchSchema = new mongoose.Schema({
	_id: {type: Number, required: true},
	name: {type: String, required: true},
	location:  { type: Location.schema },
	manager: { type: Number, ref: 'Employee', default: 1 }
});

module.exports = Branch = mongoose.model('Branch' ,branchSchema);