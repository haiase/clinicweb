import {Branch} from './branch'

export class BranchReport extends Branch {
    report: { _id: Date, cost: number, count: number}[];
}