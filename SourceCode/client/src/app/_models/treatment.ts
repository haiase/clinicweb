import { Branch } from './branch';

export class Treatment {
    _id: number;
    branch: Branch;
    carid: number;
	status: {type: string, enum : ['Waiting','In process', 'Done']};
    details: string;
    cost: number;
	date: Date;
}