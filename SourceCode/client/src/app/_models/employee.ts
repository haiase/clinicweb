import { Branch } from './branch';
import { Manager } from './manager';

export class Employee{
    _id: number;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    branch: Branch;
    manager: Manager;
    status: {type: string, enum : ['New Employee','Employee', 'Manager', 'Admin', 'None']};
    token?: string;
}