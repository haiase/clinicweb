import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { UserService, BranchService } from '@app/_services';
import { Employee } from '@app/_models';
import { SharedService } from '@app/shared/shared.service';
import { grep } from 'highcharts';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})

export class WelcomeComponent implements OnInit {
  user: Employee; 
  data:any;
  managerName:string;
  branchName:string;
  branchAddressCountry:string;
  branchAddress:string;
  branchesNum:number;
  usersNum:number;
  monthIncome:number;
  permission : string;

  constructor(private sharedService : SharedService,
    private userService : UserService,
    private branchService : BranchService,
    private http: HttpClient) {
  }

	ngOnInit() {
    this.user = this.userService.currentUserValue;
    switch(String(this.user.status)){
      case 'Admin':
        this.getAdminReport(); 
        break;
      case 'Employee':
        this.getBranchReport();
        break;
      case 'Manager':
        this.getBranchReport();
      break;
      default:
        this.http.get<Variable>(`${environment.apiUrl}/sync/${this.userService.currentUserValue._id}`, { headers: new HttpHeaders({ 'None': 'true'})})
        .pipe(first()).subscribe(data => {
          if(data['update']){
            this.userService.refreshData().subscribe(()=>{
              this.sharedService.sendLoginState(this.userService.getUserPermission());
            });
        }});
        break;
    }
  }

  getUserPermission(){
    var newpermission = this.userService.getUserPermission()
    if(newpermission != '') {
      this.permission = newpermission;
    }
    return this.permission;
  }

  getBranchReport() {
    this.branchService.getReportById(Number(this.user.branch)).pipe(first())
    .subscribe(data => {
        var greport = data[0];
        this.managerName = greport.manager.firstname + ' ' + greport.manager.lastname; 
        this.branchName = greport.name;
        this.branchAddressCountry = greport.location.country;
        this.branchAddress = greport.location.city +','+ greport.location.street;
        this.sharedService.sendBranchReport(data);
		});
  }

  getAdminReport(){
    this.branchService.getAllReports().pipe(first())
	  .subscribe(data => {
        this.data=data;
        this.branchesNum=this.data.length 
        this.usersNum=0;
        this.monthIncome=0;
        data.forEach(function (branchReport) {
         this.usersNum += branchReport.employees;
         if(branchReport.report.length)
          this.monthIncome+=branchReport.report[branchReport.report.length-1].cost;
        }.bind(this));
        this.sharedService.sendBranchReport(data);
		});
  }
}