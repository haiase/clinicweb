import { Component,OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable } from '@angular/material/sort';
import { TreatmentDialogBoxComponent } from '../dialog-box/treatment-dialog-box.component';
import { BranchService , UserService ,TreatmentService } from '@app/_services';
import { Treatment , Branch } from '@app/_models';
import { SharedService } from '@app/shared/shared.service';

@Component({
  selector: 'app-treatments',
  templateUrl: './treatments.component.html',
  styleUrls: ['./treatments.component.css']
})

export class TreatmentsComponent implements OnInit {
  displayedColumns: string[] = ['date','id','carid','cost','status','details','action'];
  dataSource = new MatTableDataSource<Treatment>();
  branchList : Branch[];
  selectedBranch : Branch;

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private dialog: MatDialog,
    private sharedService : SharedService,
    private branchService : BranchService,
    private userService : UserService,
    private treatmentService : TreatmentService){
  }

  ngOnInit() {
    if(this.allBranchesPermission())
     this.getBranches();
    else
      this.refreshTable();
	}

  allBranchesPermission(){
    return this.userService.getUserPermission() == 'Admin';
  }
  
  isSelectedBranch(){
    if(this.allBranchesPermission())
      return this.selectedBranch != null;
    else
      return true;
  }

  getBranchId(){
    if(this.allBranchesPermission())
      return this.selectedBranch._id;
    else
      return Number(this.userService.currentUserValue.branch);
  }

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(TreatmentDialogBoxComponent, {
      width: '250px',
      data:obj
    });
 
    dialogRef.afterClosed().subscribe(result => {
      if(!result) return;
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Edit'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }
 
  addRowData(row_obj){
    row_obj.branch = this.getBranchId();
    this.treatmentService.add(row_obj).pipe(first())
		.subscribe(data => {
        this.sharedService.sendAlertEvent(data);
        this.refreshTable();
		});
  }

  updateRowData(row_obj){
    row_obj.branch = this.getBranchId();
    this.treatmentService.update(row_obj).pipe(first())
		.subscribe(data => {
        this.sharedService.sendAlertEvent(data);
        this.refreshTable();
			});  
  }

  deleteRowData(row_obj){
    row_obj.branch = this.getBranchId();
    this.treatmentService.delete(row_obj._id).pipe(first())
		.subscribe(data => {
        this.sharedService.sendAlertEvent(data);
        this.refreshTable();
			});
  }

  applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  private refreshTable() {
    this.branchService.getTreatmentsById(this.getBranchId()).pipe(first())
		.subscribe(data => {
        this.dataSource.data=data;
    });
    this.sort.sort(({ id: 'date', start: 'desc'}) as MatSortable);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;  
  }

  getBranches(){
    this.branchService.getAll().pipe(first())
    .subscribe(data => {
        this.branchList=data;
    });
  }

  public loadBranch(){
    this.refreshTable();
  }
}
 