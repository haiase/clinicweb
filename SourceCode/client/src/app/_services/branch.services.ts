import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '@environments/environment'
import {Branch, BranchReport, Response, Employee, Treatment, Manager} from '../_models'

@Injectable({ providedIn: 'root' })
export class BranchService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Branch[]>(`${environment.apiUrl}/branches`);
    }

    getById(id: number) {
        return this.http.get<Branch>(`${environment.apiUrl}/branches/${id}`);
    }

    ///////////////////////////////////
    getFullById(id: number) {
        return this.http.get<Manager>(`${environment.apiUrl}/branches/${id}/full`);
    }

    getManagerById(id: number) {
        return this.http.get<Manager>(`${environment.apiUrl}/branches/${id}/manager`);
    }

    getEmployeesById(id: number) {
        return this.http.get<Employee[]>(`${environment.apiUrl}/branches/${id}/employees`);
    }

    getTreatmentsById(id: number) {
        return this.http.get<Treatment[]>(`${environment.apiUrl}/branches/${id}/treatments`);
    }

    getReportById(id: number) {
        return this.http.get<BranchReport[]>(`${environment.apiUrl}/branches/${id}/report`);
    }

    getAllReports() {
        return this.http.get<BranchReport[]>(`${environment.apiUrl}/branches/report`);
    }
    ///////////////////////////////////

    add(branch: Branch) {
        return this.http.post<Response>(`${environment.apiUrl}/branches/add`, branch);
    }

    update(branch: Branch) {
        return this.http.put<Response>(`${environment.apiUrl}/branches/${branch._id}`, branch);
    }

    delete(id: number) {
        return this.http.delete<Response>(`${environment.apiUrl}/branches/${id}`);
    }
}