const Branch = require('../models/branch');
const Counter = require('../models/counter');
const Employee = require('../models/employee');
const Treatment = require('../models/treatment');
const syncUser = require('./sync').sync;

module.exports = function (app, apiLocation) {

	//init branches schema => _id is increment key
	var counter = new Counter({_id: Branch.collection.collectionName});
	counter.save(err => {
		if(err) console.log('Counter of branches is already exists');
	});

	//get branches report
	app.get(apiLocation + '/report', function(req, res) {
		Branch.find({}, (err, result) => {
			if(err) return res.json({response : 'Error'});
			 
			async function processItems(result){
				af = [];
				for(element of result) {
					var fullBranch = element.toJSON();
					const mang = await Employee.findById(fullBranch.manager);
					fullBranch.manager = mang;
					if(fullBranch.manager)
						delete fullBranch.manager.password;
					const emps = await Employee.find({branch: fullBranch._id});
					fullBranch.employees = emps.length;
					const trtms = await getBranchReport(element._id);
					fullBranch.report = trtms;
					af.push(fullBranch);
				}
				return res.json(af);
			};
			processItems(result);
		});
	});

	//get all branches
	app.get(apiLocation, function(req, res) {	
		Branch.find({}, (err, result) => {
			if(err) return res.json({response : 'Error'});
			return res.json(result);
		});
	});
	
	//get branch by id
	app.get(apiLocation + '/:id', function(req, res) {		
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			return res.json(result);
		});
	});

	//get branch report by id
	app.get(apiLocation + '/:id/report', function(req, res) {
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			var fullBranch = result.toJSON();
			Employee.findById(result.manager, (err, result) => {
				if(err) return res.json({response : 'Error'});
				if(result){
					result = result.toJSON();
					delete result.password;
					fullBranch.manager = result;
				}
				Employee.find({branch: fullBranch._id}, async (err, result) => {
					if(err) return res.json({response : 'Error'});
					fullBranch.employees = result.length;
					const report = await getBranchReport(req.params.id);
					fullBranch.report = report;
					return res.json([fullBranch]);
				});
			});
		});
	});

	//get branch full by id
	app.get(apiLocation + '/:id/full', function(req, res) {		
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			var fullBranch = result;
			Employee.findById(result.manager, (err, result) => {
				fullBranch.manager = result;
				Employee.find({branch: result._id}, (err, result) => {
					fullBranch.employees = result;
					Treatment.find({branch: fullBranch._id}, (err, result) => {
						fullBranch.treatments = result;
						return res.json(fullBranch);
					});
				});
			});
		});
	});

	//get branch manager by id
	app.get(apiLocation + '/:id/manager', function(req, res) {		
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			Employee.findById(result.manager, (err, result) => {
				if(err) return res.json({response : 'Error'});
				return res.json(result);
			});
		});
	});

	//get branch employees by id
	app.get(apiLocation + '/:id/employees', function(req, res) {		
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});	
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			Employee.find({branch: result._id}, (err, result) => {
				if(err) return res.json({response : 'Error'});
				return res.json(result);
			});
		});
	});

	//get branch treatments by id
	app.get(apiLocation + '/:id/treatments', function(req, res) {	
		Branch.findById(Number(req.params.id), (err, result) => {
			if(err) return res.json({response : 'Error'});	
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			Treatment.find({branch: result._id}, (err, result) => {
				if(err) return res.json({response : 'Error'});
				return res.json(result);
			});
		});
	});

	//add branch
	app.post(apiLocation + '/add', function(req, res) {
		Counter.findByIdAndUpdate(counter, { $inc: { seq: 1 } }, { new: true }, (err, result) => {
			if(err) return res.json({response : 'Error'});
			req.body._id = Number(result.seq);
			var newBranch =  new Branch(req.body);
			newBranch.save(err => {
				if(err) return res.json({response : 'Error'})
	
				return res.json({response : 'Success', msg : 'Successfully added branch'});
			});
		});
	});

	//update branch
	app.put(apiLocation + '/:id', function(req, res) {	
		req.body._id = Number(req.params.id);
		var updateBranch =  new Branch(req.body);
		Branch.findByIdAndUpdate(Number(req.params.id), { $set: updateBranch }, (err, result) => {
			if(err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 
			return res.json({response : 'Success', msg : 'Branch number ' + updateBranch._id + ' has been updated'}); 
		});
	});
	
	//delete branch
	app.delete(apiLocation + '/:id', function(req, res) {
		Branch.findByIdAndRemove(Number(req.params.id), (err, result) => {
			if (err) return res.json({response : 'Error'});
			if(result == null) return res.json({response : 'Error', msg : 'Branch doesnt exist'}); 

			Employee.find({branch: Number(req.params.id)}, (err, result) => {
				if (err) return res.json({response : 'Error'});
				async function processItems(result){
					for(element of result) {
						await element.updateOne({ $set: { status: 'None', branch: null }});
						syncUser(element._id);
					}
					return res.json({response : 'Success', msg : 'Branch number ' + Number(req.params.id) + ' has been deleted'}); 
				};
				processItems(result);
			});
		});
	});
};

async function getBranchReport(branchid){
	var lastyear = new Date(Date.now());
	lastyear.setMonth(lastyear.getMonth() - 12);
	var now = new Date(Date.now());
	return await Treatment.aggregate([
		{
			$match: {
				branch: Number(branchid),
			},
		},
		{
			$match: {
				date: {
					$gte: lastyear,
					$lte: now
				}
			},
		},
		{
			$project: {
				id: "$branch",
				datemy: {$dateFromParts: {'year' : {$year: "$date"}, 'month' : {$month: "$date"}}},
				cost: "$cost"
			}
		},
		{
			$group: {
				_id : "$datemy",
				count: {
					$sum: { "$sum" : 1 } ,
				},
				cost: {
					$sum: { "$sum" : "$cost" } ,
				}
			},
		},
		{ $sort : {_id: 1}}
	], (err, result) => {
		return result;
	});
}