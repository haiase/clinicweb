const projectConfig = {
	db: {
		url: "mongodb://localhost:27017/",
		name: "clinic"
	},
	port: 3000,
	jwtSecret: "clinicproject12312322",
	email: {
		username: "***email***@gmail.com",
		password: "***password***"
	}
};

module.exports = projectConfig;